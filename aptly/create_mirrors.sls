# Set up our Aptly mirrors


{% from "aptly/map.jinja" import aptly with context %}

include:
  - aptly
  - aptly.aptly_config

{% set distribution = aptly.default_distribution %}
{% if salt['pillar.get']('aptly:mirrors') %}
{% for mirror, opts in salt['pillar.get']('aptly:mirrors').items() %}
  {% set mirrorloop = mirror %}
  {%- if opts['url'] is defined -%}
    {# if we have a url parameter #}
    {%- set create_mirror_cmd = "aptly mirror create -architectures='" ~ opts['architectures']|default([])|join(',') ~ "' " ~ mirror ~ " " ~ opts['url'] ~ " " ~ opts['distribution']|default('') ~ " " ~ opts['components']|default([])|join(' ') -%}
  {% elif opts['ppa'] is defined %}
    {# otherwise, if we have a ppa parameter  #}
    {%- set create_mirror_cmd = "aptly mirror create -architectures='" ~ opts['architectures']|default([])|join(',') ~ "' " ~ mirror ~ " ppa:" ~ opts['ppa'] -%}
  {% endif %}

create_{{ mirror }}_mirror:
  cmd.run:
    - name: {{ create_mirror_cmd }}
    - unless: aptly mirror show {{ mirror }}
    - runas: aptly
    - env:
      - HOME: {{ aptly.homedir }}
    - require:
      - sls: aptly.aptly_config
{% if opts['keyids'] is defined %}
{% for keyid in opts['keyids'] %}
      - cmd: add_{{ mirrorloop }}_{{ keyid }}_gpg_key
{% endfor %}

{% for keyid in opts['keyids'] %}

add_{{ mirrorloop }}_{{ keyid }}_gpg_key:
  cmd.run:
    - name: {{ aptly.gpg_command }} --no-default-keyring --keyring {{ aptly.gpg_keyring }} --keyserver {{ opts['keyserver']|default('keys.gnupg.net') }} --recv-keys {{keyid}}
    - runas: aptly
    - unless: {{ aptly.gpg_command }} --list-keys --keyring {{ aptly.gpg_keyring }}  | grep {{ keyid }}
{% endfor %}
  {% elif opts['keyid'] is defined %}
      - cmd: add_{{ mirror }}_gpg_key

add_{{ mirror }}_gpg_key:
  cmd.run:
    - name: {{ aptly.gpg_command }} --no-default-keyring --keyring {{ aptly.gpg_keyring }} --keyserver {{ opts['keyserver']|default('keys.gnupg.net') }} --recv-keys {{ opts['keyid'] }}
    - runas: aptly
    - unless: {{ aptly.gpg_command }} --list-keys --keyring {{ aptly.gpg_keyring }}  | grep {{ opts['keyid'] }}
  {% elif opts['key_url'] is defined %}
      - cmd: add_{{ mirror }}_gpg_key

add_{{ mirror }}_gpg_key:
  cmd.run:
    - name: {{ aptly.gpg_command }} --no-default-keyring --keyring {{ aptly.gpg_keyring }} --fetch-keys {{ opts['key_url'] }}
    - runas: aptly
    - unless: {{ aptly.gpg_command }} --list-keys --keyring {{ aptly.gpg_keyring }}  | grep {{ keyid }}
  {% endif %}


update_{{ mirror }}_mirror:
  cmd.run:
    - name: aptly mirror -keyring={{ aptly.gpg_keyring }} update {{ mirror }} 
    - runas: aptly
    - env:
      - HOME: {{ aptly.homedir }}
    - require:
      - sls: aptly.aptly_config
    - failhard: True

unpublish_{{ mirror }}_old_snapshot:
   cmd.run:
    - name: aptly publish drop {{ mirror }}-old
    - onlyif: aptly publish show {{ mirror }}-old
    - runas: aptly
    - env:
      - HOME: {{ aptly.homedir }}
    - require:
      - sls: aptly.aptly_config

unpublish_{{ mirror }}_snapshot:
   cmd.run:
    - name: aptly publish drop {{ mirror }}
    - onlyif: aptly publish show {{ mirror }}
    - runas: aptly
    - env:
      - HOME: {{ aptly.homedir }}
    - require:
      - sls: aptly.aptly_config

delete_{{ mirror }}_old_snapshot:
   cmd.run:
    - name: aptly snapshot drop {{ mirror }}-old
    - onlyif: aptly snapshot show {{ mirror }}-old
    - runas: aptly
    - env:
      - HOME: {{ aptly.homedir }}
    - require:
      - sls: aptly.aptly_config

rename_{{ mirror }}_snapshot_latest_to_old:
  cmd.run:
    - name: aptly snapshot rename {{ mirror }}-latest {{ mirror }}-old
    - onlyif: aptly snapshot show {{ mirror }}-latest
    - runas: aptly
    - env:
      - HOME: {{ aptly.homedir }}
    - require:
      - sls: aptly.aptly_config
      - update_{{ mirror }}_mirror

publish_{{ mirror }}_old_snapshot:
  cmd.run:
    - name: aptly publish snapshot -label="$(cat {{ aptly.patchlevel_file }})" -distribution={{ mirror }}-old {{ mirror }}-old
    - onlyif: aptly snapshot show {{ mirror }}-old
    - runas: aptly
    - env:
    - HOME: {{ aptly.homedir }}
    - require:
      - sls: aptly.aptly_config

create_{{ mirror }}_latest_snapshot:
  cmd.run:
    - name: aptly snapshot create "{{ mirror }}-latest" from mirror {{ mirror }}
    - runas: aptly
    - env:
      - HOME: {{ aptly.homedir }}
    - require:
      - sls: aptly.aptly_config

{% endfor %}
{% endif %}
