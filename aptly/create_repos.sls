# Set up our Aptly repos
{% from "aptly/map.jinja" import aptly with context %}

include:
  - aptly
  - aptly.aptly_config

{% for repo, opts in salt['pillar.get']('aptly:repos').items() %}
  {% for distribution in opts['distributions'] %}
    {% for component in opts['components'] %}
      {% set repo_name = repo + '_' + distribution + '_' + component %}
      {% set full_path = opts['pkgdir'] + '/' + distribution + '/' + component %}

create_patchlevel_deb:
  cmd.run:
    - name: |
        cd {{ aptly.homedir }}
        PATCHLEVEL=$(cat {{ aptly.patchlevel_file }})
        DEBDIR=patchlevel-1.0-$PATCHLEVEL
        mkdir -p $DEBDIR/{DEBIAN,etc}
        echo $PATCHLEVEL > $DEBDIR/etc/patchlevel
        echo "Package: patchlevel" > $DEBDIR/DEBIAN/control
        echo "Version: 1.0-$PATCHLEVEL" >> $DEBDIR/DEBIAN/control
        echo "Section: base" >> $DEBDIR/DEBIAN/control
        echo "Priority: optional" >> $DEBDIR/DEBIAN/control
        echo "Architecture: amd64" >> $DEBDIR/DEBIAN/control
        echo "Depends: " >> $DEBDIR/DEBIAN/control
        echo "Maintainer: Deposit Solutions Internal Package Repository <infrastructure@deposit-solutions.com" >> $DEBDIR/DEBIAN/control
        echo "Description: Patchlevel" >> $DEBDIR/DEBIAN/control
        dpkg-deb --build $DEBDIR
        mv $DEBDIR.deb /srv/pkgrepo/internalpkgs/stretch/deposit/$DEBDIR.deb
        rm -rf $DEBDIR
    - unless: ls {{ full_path }}/patchlevel-1.0-$(cat {{ aptly.patchlevel_file }})

create_{{ repo_name }}_repo:
  cmd.run:
    - name: aptly repo create -distribution="{{ distribution }}" -comment="{{ opts['comment'] }}" -component="{{ component }}" {{ repo_name }}
    - unless: aptly repo show {{ repo_name }}
    - runas: aptly
    - env:
      - HOME: {{ aptly.homedir }}
    - require:
      - sls: aptly.aptly_config

      {% if opts.get('pkgdir', false) %}
{{ opts['pkgdir'] }}/{{ distribution }}/{{ component }}:
  file.directory:
    - user: root
    - group: root
    - mode: 777
    - makedirs: True
        {% set numcurrentpkgs = salt['cmd.run']('aptly repo show ' ~ repo_name ~ ' | tail -n1 | cut -f4 -d \' \'', user='aptly', env="[{\'HOME\':\'' ~ homedir ~ '\'}]") %}
        {% set pkgsinpkgdir = salt['file.find'](' ~ opts["pkgdir"] ~ ', type='f', iregex='.*(deb|udeb|dsc)$')|count %}
        {% if numcurrentpkgs != pkgsinpkgdir %}
          {# we dont  have all the packages loaded, add all packages in opts['pkgdir'] #}

add_{{ repo_name }}_pkgs:
  cmd.run:
    - name: aptly repo add -force-replace=true -remove-files=true {{ repo_name }} {{ opts['pkgdir'] }}/{{ distribution }}/{{ component }}
    - runas: aptly
    - env:
      - HOME: {{ aptly.homedir }}
    - onlyif:
      - find {{ opts['pkgdir'] }}/{{ distribution }}/{{ component }} -type f -mindepth 1 -print -quit | grep -q .
    - require:
      - cmd: create_{{ repo_name }}_repo
        {% endif %}
      {% endif %}
    {% endfor %}
  {% endfor %}
{% endfor %}
